import Header from "./components/header/Header.jsx";
import {useEffect, useState} from "react";
import {Button, MenuItem, Select, TextField} from "@mui/material";
import classes from './main.module.scss'
import moment from "moment";
import {useDispatch, useSelector} from "react-redux";
import {setModal} from "./store/modalSlice.js";
import Modal from "./components/modal/Modal.jsx";
import {addPost, deletePost} from "./api/api.js";
import {DateCalendar, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterMoment} from "@mui/x-date-pickers/AdapterMoment";
import Shure from "./components/shure/Shure.jsx";
import {setShure} from "./store/shureSlice.js";

const App = () => {
    const modal = useSelector(state => state.modal.modal)
    const shure = useSelector(state => state.shure.shure)
    const dispatch = useDispatch()

    const [post, setPost] = useState({
        id: Math.random() * 1000,
        date: new Date(),
        name: "",
        tags: [],
        type: "anime",
        text: [],
        prev: ""
    })
    const [tag, setTag] = useState('')
    const [deleteInput, setDeleteInput] = useState("")
    useEffect(() => {
        console.log(post)
    }, [post]);

    const handleType = (e) => {
        setPost({...post, type: e.target.value})
    }
    const addTag = () => {
        if (tag.length > 1) {
            const a = post.tags.slice()
            a.push(tag)
            setPost({...post, tags: a})
            setTag('')
        }
    }
    const deleteTag = (e) => {
        const a = post.tags.slice()
        setPost({
            ...post, tags: a.filter(t => {
                if (t !== e) {
                    return t
                }
            })
        })
    }
    const addElement = (element) => {
        if (element["data"].length > 0) {
            const a = post.text.slice()
            a.push(element)
            setPost({...post, text: a})
        }
    }
    const handleAddPost = async () => {
        if (post.name && post.prev && post.text.length) {
            await addPost(post)
            setPost({
                id: Math.random() * 1000,
                date: new Date(),
                name: "",
                tags: [],
                type: "anime",
                text: [],
                prev: ""
            })
        }
    }
    const handleShure = () => {
        dispatch(setShure(!shure))
    }
    const handleModal = () => {
        dispatch(setModal(true))
    }
    const deleteElem = (element) => {
        const a = post.text.slice()
        setPost({
            ...post, text: a.filter(e => {
                return e !== element
            })
        })
    }
    const handleDate = (e) => {
        setPost({...post, date: e._d})
    }
    const handleDelete = () => {
        if (deleteInput.length > 0) {
            deletePost(deleteInput)
            setDeleteInput("")
        }
    }

    return (
        <>
            {modal && <Modal addElement={addElement}/>}
            {shure && <Shure handleShure={handleShure} handleAddPost={handleAddPost}/>}
            <Header/>
            <div className={classes.main}>
                <TextField variant='outlined'
                           value={post.name}
                           label='Заголовок'
                           onChange={e => setPost({...post, name: e.target.value})}/>
                <LocalizationProvider dateAdapter={AdapterMoment}>
                    <DateCalendar value={moment(post.date)} onChange={handleDate} disableFuture/>
                </LocalizationProvider>
                <Select
                    labelId="demo-simple-select-label"
                    placeholder='Тип'
                    id="demo-simple-select"
                    value={post.type}
                    onChange={handleType}>
                    <MenuItem value='anime'>Аніме</MenuItem>
                    <MenuItem value='game'>Ігри</MenuItem>
                    <MenuItem value='film'>Фільми</MenuItem>
                    <MenuItem value='other'>Інше...</MenuItem>
                </Select>
                <TextField variant='outlined'
                           value={post.prev}
                           label='Головна Заставка'
                           onChange={e => setPost({...post, prev: e.target.value})}/>
                <TextField variant='outlined'
                           value={tag}
                           label='Тег'
                           onChange={e => setTag(e.target.value)}/>
                <Button variant='contained' onClick={addTag}>+ Тег</Button>
                <div className={classes.tags}> {post.tags.map((e, i) => {
                    return <div key={i}><p>#{e}</p>
                        <button onClick={() => deleteTag(e)}>x</button>
                    </div>
                })}</div>
                <div className={classes.post}>
                    <h3 className={classes.date}>{moment(post.date).format("DD.MM.YYYY")}</h3>
                    <h2 className={classes.title}>{post.name}</h2>
                    <div className={classes.tags}>{post.tags.map((e, i) => {
                        return <p key={i}>#{e}</p>
                    })}</div>

                    {post.text.map((e, i) => {
                        return <div key={i} className={classes.block}>
                            <div className={classes.closed} onClick={() => deleteElem(e)}>x</div>
                            {e['regim'] === "text" && <p className={classes.text}>{e["data"]}</p>}
                            {e['regim'] === "big" && <h3 className={classes.big}>{e["data"]}</h3>}
                            {e['regim'] === "img" && <img className={classes.img} src={e["data"]} alt=''/>}
                            {e['regim'] === "youtube" && <iframe className={classes.video} src={e["data"]}
                                title="YouTube video player" frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;
                                picture-in-picture; web-share"
                                allowFullScreen={true}></iframe>}
                        </div>
                    })}
                    <Button variant='contained' onClick={handleModal} className={classes.add}>+ Елемнт</Button>
                </div>
                <Button variant='outlined' onClick={handleShure}>Додати пост</Button>
                <TextField variant='outlined'
                           value={deleteInput}
                           label='Назва поста який потрібно виалити'
                           onChange={e => setDeleteInput(e.target.value)}/>
                <Button variant='outlined' onClick={handleDelete}>Видалити пост</Button>
            </div>
        </>
    )
}

export default App
