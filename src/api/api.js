import {supabase} from "./config.js";

export const addPost = async ({id, date, name, tags, type, text, prev}) => {
    await supabase.from('posts').insert({
        id: Math.floor(id),
        date: date,
        name: name,
        tags: tags,
        type: type,
        text: text,
        prev: prev
    })
}
export const deletePost = async (name) => {
    await supabase.from('posts').delete().eq("name", name)
}
