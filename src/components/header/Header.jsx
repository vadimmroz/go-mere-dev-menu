import classes from './header.module.scss'

const Header = () => {
    return (
        <header className={classes.header}>
            <img src="https://i.ibb.co/GVYNWB9/Ellipse-1-removebg-preview.webp" alt=""/>
            <h1>GO-MERE.COM</h1>
        </header>
    );
};

export default Header;