import {useState} from 'react';
import classes from './modal.module.scss'
import {Button, Card, MenuItem, Select, TextField} from "@mui/material";
import {useDispatch} from "react-redux";
import {setModal} from "../../store/modalSlice.js";

const Modal = ({addElement}) => {
    const [regime, setRegime] = useState('text')
    const [input, setInput] = useState('')

    const dispatch = useDispatch()

    const handleSelect = (e) => {
        setRegime(e.target.value)
    }
    const handleClosed = () => {
        dispatch(setModal(false))
    }
    const handleInput = (e) => {
        setInput(e.target.value)
    }
    const add = () => {
        if (input.length > 0) {
            setInput("")
            setRegime("text")
            if (regime === 'youtube') {
                let a = input
                let help = false
                let b = ""
                for (let i = 0; i < a.length; i++) {
                    if (help) {
                        b += a[i]
                    }
                    if (a[i] === "=") {
                        help = true
                    }
                }
                a = "https://www.youtube.com/embed/" + b
                addElement({"regim": regime, "data": a})
            } else {
                addElement({"regim": regime, "data": input})
            }
            dispatch(setModal(false))
        }
    }

    return (
        <div className={classes.modal} onClick={handleClosed}>
            <Card className={classes.card} onClick={e => {
                e.preventDefault();
                e.stopPropagation()
            }}>
                <Select placeholder='Тип' value={regime} onChange={handleSelect}>
                    <MenuItem value='text'>Текст</MenuItem>
                    <MenuItem value='big'>Великий Текст</MenuItem>
                    <MenuItem value='img'>Картинка</MenuItem>
                    <MenuItem value='youtube'>Відео з Ютуб</MenuItem>
                </Select>
                <TextField onChange={handleInput} value={input} placeholder={regime}/>
                <Button variant='contained' onClick={add}>+</Button>
            </Card>
        </div>
    );
};

export default Modal;