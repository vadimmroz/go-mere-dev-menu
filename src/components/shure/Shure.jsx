import classes from './shure.module.scss'
import {Button, Card} from "@mui/material";

const Shure = ({handleShure, handleAddPost}) => {
    const handleAdd = () => {
        handleShure()
        handleAddPost()
    }
    return (
        <div className={classes.shure} onClick={handleShure}>
            <Card className={classes.card} onClick={e => {
                e.preventDefault();
                e.stopPropagation()
            }}>
                <h2>Впевнений(на)?</h2>
                <Button variant='contained' onClick={handleAdd}>Так</Button>
                <Button variant='outlined' onClick={handleShure}>Ні</Button>
            </Card>
        </div>
    );
};

export default Shure;