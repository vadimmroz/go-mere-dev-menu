import {createSlice} from "@reduxjs/toolkit";

const shureSlice = createSlice({
    name: 'shure',
    initialState:{
        shure: false
    },
    reducers:{
        setShure(state, action){
            state.shure = action.payload
        }
    }
})
export const {setShure} = shureSlice.actions
export default shureSlice.reducer