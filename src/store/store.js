import {configureStore} from "@reduxjs/toolkit";
import modalSlice from "./modalSlice.js";
import shureSlice from "./shureSlice.js";
export default configureStore({
    reducer:{
        modal: modalSlice,
        shure: shureSlice
    }
})